# 灵动岛效果

# iPhone 12 网页文字渐入效果

代码来源：https://codepen.io/stevenlei/pen/MWeKVqP

主要代码：

```css
background-image: linear-gradient(75deg, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 1) 33.33%, rgba(255, 255, 255, 0) 66.67%, rgba(255, 255, 255, 0) 100%);
background-position-x: calc(100% - var(--percentage));
background-clip: text;
-webkit-background-clip: text;
color: transparent;
background-size: 300% 100%;
```

> 文字颜色透明，通过背景图来显示颜色渐变的效果。background-position-x 通过控制背景图的位置来实现颜色的显示与消失。背景图的位置信息与页面滚动的距离有关。



# iPhone 12 页面卷动逐行滑入效果

代码来源：https://codepen.io/stevenlei/pen/ExydYqp

主要代码

```css
transform: scale(calc(1.8 - (0.8 * var(--progress)))) translateY(calc(-60px * (1 - var(--progress))));
opacity: var(--progress);
```

> 列表项向下滚动的时候，存在三个变化，1.放大（通过 scale 来实现）2.向下移动（通过 translateY来实现）3.透明度增大（通过 opacity 来实现）。
>
> 计算出每一项的开始和结束占整个滚动的百分比，之后计算出每一项滚动距离占开始和结束的百分比。

# iPhone SE 产品网页之颜色切换特效

代码来源：https://codepen.io/stevenlei/pen/pojRJYq

主要代码：

```css
position: absolute;
top: 0;
left: 0;
```

```css
clip-path: inset(100% 0px 0px 0px);
```

> 三张图片本来是重合在一起，clip-path 将图片裁剪出来。